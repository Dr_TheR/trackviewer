package ru.worldtillfuture.trackviewer;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import ru.worldtillfuture.trackviewer.database.DatabaseManager;
import ru.worldtillfuture.trackviewer.database.base.DatabaseHelper;
import ru.worldtillfuture.trackviewer.database.ormlite.ORMLiteHelper;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseHelper helper = new ORMLiteHelper(getApplicationContext());
        DatabaseManager.init(helper);
    }

    @Override
    public void onTerminate() {
        DatabaseManager.release();
        super.onTerminate();
    }
}