package ru.worldtillfuture.trackviewer.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

import ru.worldtillfuture.trackviewer.database.base.entity.DatabaseEntity;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
@DatabaseTable(tableName = "gpx_info")
public class GpxInfo implements DatabaseEntity, Serializable {
    public static final String LAST_ACTIVITY_DATE_COLUMN = "last_activity_date";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String filePath;

    @DatabaseField
    private String name;

    @DatabaseField(dataType = DataType.DATE, columnName = LAST_ACTIVITY_DATE_COLUMN)
    private Date lastActivityDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(Date lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public void updateDate() {
        lastActivityDate = new Date();
    }

    public GpxInfo() {
    }

    public GpxInfo(String filePath, String name) {
        this.filePath = filePath;
        this.name = name;
        this.lastActivityDate = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEntityName() {
        return toString()+"#"+ id;
    }
}
