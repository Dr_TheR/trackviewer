package ru.worldtillfuture.trackviewer.data;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.urizev.gpx.beans.Track;
import com.urizev.gpx.beans.Waypoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.worldtillfuture.trackviewer.utilities.ColorUtilities;

/**
 * Author: Ignatov Pavel
 * Date: 06.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class TrackInfo {
    private static int lastId = 0;

    private int id;
    private int mColor;
    private List<LatLng> mPoints;
    private Track mTrack;
    private LatLngBounds mBounds;

    private Marker mStartMarker;
    private Marker mFinishMarker;

    private Polyline mTrackPolyline;

    public TrackInfo(Track track){
        id = lastId++;
        mTrack = track;
        mPoints = new ArrayList<>();
        mColor = ColorUtilities.generateRandomColor();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<Waypoint> waypoints = track.getTrackPoints();
        for (Waypoint waypoint : waypoints){
            LatLng point = new LatLng(waypoint.getLatitude(), waypoint.getLongitude());
            builder.include(point);
            mPoints.add(point);
        }
        mBounds = builder.build();
    }

    public int getColor() {
        return mColor;
    }

    public Track getTrack() {
        return mTrack;
    }

    public int getId() {
        return id;
    }

    public LatLng getStartPoint(){
        if (mPoints == null || mPoints.size() <= 0)
            return null;

        LatLng result = mPoints.get(0);

        return result;
    }

    public LatLng getFinishPoint(){
        if (mPoints == null || mPoints.size() <= 0)
            return null;

        int lastIndex = mPoints.size()-1;
        LatLng result = mPoints.get(lastIndex);

        return result;
    }

    public Marker getStartMarker() {
        return mStartMarker;
    }

    public void setStartMarker(Marker startMarker) {
        this.mStartMarker = startMarker;
    }

    public Marker getFinishMarker() {
        return mFinishMarker;
    }

    public void setFinishMarker(Marker finishMarker) {
        this.mFinishMarker = finishMarker;
    }

    public Polyline getTrackPolyline() {
        return mTrackPolyline;
    }

    public void setTrackPolyline(Polyline trackPolyline) {
        this.mTrackPolyline = trackPolyline;
        mTrackPolyline.setPoints(mPoints);
    }

    public LatLngBounds getBounds() {
        return mBounds;
    }
}
