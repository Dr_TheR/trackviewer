package ru.worldtillfuture.trackviewer.database;

import android.util.Log;

import ru.worldtillfuture.trackviewer.database.base.DatabaseHelper;
import ru.worldtillfuture.trackviewer.database.base.dao.GpxInfoDAO;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class DatabaseManager{
    public static final String DATABASE_LOG_TAG = "database";

    public static final String DATABASE_NAME = "tracks.db";
    public  static final int DATABASE_VERSION = 19;

    private static DatabaseHelper mHelper;

    private DatabaseManager(){}

    public static void init(DatabaseHelper helper){
        mHelper = helper;
    }

    public static GpxInfoDAO getGpxInfoDAO() {
        if (mHelper == null){
            throw new RuntimeException("Error: Database is not initialize");
        }
        return mHelper.getGpxInfoDAO();
    }

    public static void release(){
        if (mHelper != null)
            mHelper.release();

        mHelper = null;
    }
}
