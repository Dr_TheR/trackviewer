package ru.worldtillfuture.trackviewer.database.base;

import ru.worldtillfuture.trackviewer.database.base.dao.GpxInfoDAO;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public interface DatabaseHelper {
    GpxInfoDAO getGpxInfoDAO();

    void release();
}
