package ru.worldtillfuture.trackviewer.database.base.dao;

import java.util.List;

import ru.worldtillfuture.trackviewer.database.base.entity.DatabaseEntity;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public interface DatabaseDAO<T extends DatabaseEntity> {
    boolean saveElement(T element);
    boolean deleteElement(T element);
    List<T> getAllElements();
}
