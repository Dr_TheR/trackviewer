package ru.worldtillfuture.trackviewer.database.base.dao;

import java.util.List;

import ru.worldtillfuture.trackviewer.data.GpxInfo;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public interface GpxInfoDAO extends DatabaseDAO<GpxInfo> {
}
