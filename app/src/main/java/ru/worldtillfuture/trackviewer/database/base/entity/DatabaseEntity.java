package ru.worldtillfuture.trackviewer.database.base.entity;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public interface DatabaseEntity {
    String getEntityName();
}
