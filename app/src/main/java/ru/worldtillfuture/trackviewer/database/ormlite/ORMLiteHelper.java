package ru.worldtillfuture.trackviewer.database.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.database.DatabaseManager;
import ru.worldtillfuture.trackviewer.database.base.DatabaseHelper;
import ru.worldtillfuture.trackviewer.database.base.dao.GpxInfoDAO;
import ru.worldtillfuture.trackviewer.database.ormlite.dao.GpxInfoDAOImpl;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class ORMLiteHelper extends OrmLiteSqliteOpenHelper implements DatabaseHelper{
    private static final String DATABASE_NAME = DatabaseManager.DATABASE_NAME;
    private static final int DATABASE_VERSION = DatabaseManager.DATABASE_VERSION;
    private static final String TAG = DatabaseManager.DATABASE_LOG_TAG;

    private GpxInfoDAO mGpxInfoDAO;

    public ORMLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try
        {
            TableUtils.createTable(connectionSource, GpxInfo.class);
        }
        catch (SQLException e){
            Log.e(TAG, "Error: Creating DB with name = " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            //TODO Менять БД, а не удалять полностью
            TableUtils.dropTable(connectionSource, GpxInfo.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        }
        catch (SQLException e){
            Log.e(TAG, "Error: Updating DB with name = " + DATABASE_NAME + " from version = " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    @Override
    public GpxInfoDAO getGpxInfoDAO() {
        try {
            if (mGpxInfoDAO == null)
                mGpxInfoDAO = new GpxInfoDAOImpl(getConnectionSource(), GpxInfo.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mGpxInfoDAO;
    }

    @Override
    public void release() {
        OpenHelperManager.releaseHelper();
    }


    @Override
    public void close(){
        super.close();
        mGpxInfoDAO = null;
    }
}
