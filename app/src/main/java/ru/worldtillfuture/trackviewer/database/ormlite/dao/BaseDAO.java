package ru.worldtillfuture.trackviewer.database.ormlite.dao;

import android.util.Log;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.worldtillfuture.trackviewer.database.DatabaseManager;
import ru.worldtillfuture.trackviewer.database.base.dao.DatabaseDAO;
import ru.worldtillfuture.trackviewer.database.base.entity.DatabaseEntity;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public abstract class BaseDAO<T extends DatabaseEntity> extends BaseDaoImpl<T, Integer> implements DatabaseDAO<T> {
    protected BaseDAO(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public boolean saveElement(T element) {
        try {
            createOrUpdate(element);
            return true;
        } catch (SQLException e) {
            Log.e(DatabaseManager.DATABASE_LOG_TAG, "Error: In element " + element.getEntityName() + " " + e.getMessage());
        }

        return false;
    }

    @Override
    public boolean deleteElement(T element) {
        try {
            delete(element);
            return true;
        } catch (SQLException e) {
            Log.e(DatabaseManager.DATABASE_LOG_TAG, "Error: In element "+ element.getEntityName() + " " + e.getMessage());
        }
        return false;
    }

    @Override
    public List<T> getAllElements() {
        try {
            return this.queryForAll();
        } catch (SQLException e) {
            Log.e(DatabaseManager.DATABASE_LOG_TAG, "Error:" + e.getMessage());
        }

        return null;
    }
}