package ru.worldtillfuture.trackviewer.database.ormlite.dao;

import android.util.Log;

import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.database.DatabaseManager;
import ru.worldtillfuture.trackviewer.database.base.dao.GpxInfoDAO;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class GpxInfoDAOImpl extends BaseDAO<GpxInfo> implements GpxInfoDAO {

    public GpxInfoDAOImpl(ConnectionSource connectionSource, Class<GpxInfo> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public List<GpxInfo> getAllElements() {
        List<GpxInfo> gpxInfos = null;
        try {
            QueryBuilder<GpxInfo, Integer> builder = queryBuilder();
            builder.orderBy(GpxInfo.LAST_ACTIVITY_DATE_COLUMN, false);
            gpxInfos = query(builder.prepare());
        } catch (Exception e) {
            Log.e(DatabaseManager.DATABASE_LOG_TAG, "Error:" + e.getMessage());
        }
        return gpxInfos;
    }
}