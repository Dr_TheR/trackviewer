package ru.worldtillfuture.trackviewer.gpx.parse;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.urizev.gpx.beans.GPX;

import java.util.List;

import ru.worldtillfuture.trackviewer.data.TrackInfo;

/**
 * Author: Ignatov Pavel
 * Date: 07.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class GpxParseHandler extends Handler {
    public static final String GPX_PARSE_RESULT_DATA = "gpx_parse_result";
    public static final String GPX_PARSE_STATUS_DATA = "gpx_parse_status";

    private GpxParseCallback mCallback;

    public GpxParseHandler(GpxParseCallback callback) {
        mCallback = callback;
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);

        GpxParseStatus status = (GpxParseStatus) msg.getData().get(GPX_PARSE_STATUS_DATA);

        if(status == GpxParseStatus.SUCCESS){
            GpxParseProcessing.GpxParseResult resultData = (GpxParseProcessing.GpxParseResult) msg.getData().get(GPX_PARSE_RESULT_DATA);
            mCallback.onParsingSuccess(resultData.getGpx(), resultData.getTracks());
        }
        else{
            mCallback.onParsingError(status);
        }

    }

    public static void sendMessage(GpxParseStatus status, GpxParseProcessing.GpxParseResult result, Handler handlerReceiver){
        Message msg = handlerReceiver.obtainMessage();
        Bundle bundle = new Bundle();

        bundle.putSerializable(GpxParseHandler.GPX_PARSE_STATUS_DATA, status);
        bundle.putSerializable(GpxParseHandler.GPX_PARSE_RESULT_DATA, result);

        msg.setData(bundle);
        handlerReceiver.sendMessage(msg);
    }

    public interface GpxParseCallback{
        void onParsingSuccess(GPX gpx, List<TrackInfo> tracks);
        void onParsingError(GpxParseStatus status);
    }
}
