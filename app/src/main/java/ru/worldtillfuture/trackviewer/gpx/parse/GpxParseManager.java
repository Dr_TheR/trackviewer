package ru.worldtillfuture.trackviewer.gpx.parse;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class GpxParseManager {
    public static final String GPX_PARSE_LOG_TAG = "gpx_parse";
    public static GpxParseProcessing parseGPX(File file, Handler handler){
        GpxParseProcessing processing = null;
        try {
            FileInputStream stream = new FileInputStream(file);
            processing = new GpxParseProcessing(stream, handler);
            Thread thread = new Thread(processing);
            thread.start();
        } catch (FileNotFoundException e) {
            Log.e(GPX_PARSE_LOG_TAG, "Error: An GpxParseManager FileNotFoundException " + e.getMessage());
            GpxParseHandler.sendMessage(GpxParseStatus.FILE_NOT_FOUND, null, handler);
        } catch (Exception e) {
            Log.e(GPX_PARSE_LOG_TAG, "Error: An GpxParseManager " + e.getMessage());
            GpxParseHandler.sendMessage(GpxParseStatus.PARSE_ERROR, null, handler);
        }

        return processing;
    }
}
