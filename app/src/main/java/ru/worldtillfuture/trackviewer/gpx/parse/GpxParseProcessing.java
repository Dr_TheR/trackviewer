package ru.worldtillfuture.trackviewer.gpx.parse;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.urizev.gpx.GPXParser;
import com.urizev.gpx.beans.GPX;
import com.urizev.gpx.beans.Track;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.worldtillfuture.trackviewer.data.TrackInfo;

/**
 * Author: Ignatov Pavel
 * Date: 07.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class GpxParseProcessing implements Runnable{
    FileInputStream mStream;
    private volatile Handler mHandler;
    private GPX mGpx;
    private List<TrackInfo> mTracks;
    private GpxParseStatus mStatus = GpxParseStatus.IS_NOT_STARTED;

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public GpxParseProcessing(FileInputStream stream, Handler handler) {
        mStream = stream;
        mHandler = handler;
    }

    public GPX getGpx() {
        GPX gpx = mGpx;
        return gpx;
    }

    public List<TrackInfo> getTracks() {
        List<TrackInfo> tracks = mTracks;
        return tracks;
    }

    @Override
    public void run() {
        mGpx = null;
        mTracks = null;

        mStatus = GpxParseStatus.RUNNING;
        mStatus = parse();

        if (mStatus != GpxParseStatus.SUCCESS) {
            sendErrorMessage(mStatus);
            return;
        }

        mStatus = generateTrackInfoList();

        if (mStatus != GpxParseStatus.SUCCESS){
            sendErrorMessage(mStatus);
            return;
        }

        sendSuccessMessage(mGpx, mTracks);
    }

    public GpxParseStatus getStatus(){
        GpxParseStatus status = mStatus;
        return status;
    }

    public void sendErrorMessage(GpxParseStatus status){
        Handler handler = mHandler;

        GpxParseHandler.sendMessage(status, null, handler);
    }

    private void sendSuccessMessage(GPX gpx, List<TrackInfo> tracks){
        Handler handler = mHandler;

        GpxParseResult resultData = new GpxParseResult();
        resultData.mGpx = mGpx;
        resultData.mTracks = mTracks;

        GpxParseHandler.sendMessage(GpxParseStatus.SUCCESS, resultData, handler);
    }

    private GpxParseStatus parse(){
        GpxParseStatus status = GpxParseStatus.RUNNING;

        GPXParser parser = new GPXParser();

        try {
            mGpx = parser.parseGPX(mStream);
        } catch (FileNotFoundException e) {
            status = GpxParseStatus.FILE_NOT_FOUND;
            Log.e(GpxParseManager.GPX_PARSE_LOG_TAG, "Error: An GpxParseManager FileNotFoundException " + e.getMessage());
        } catch (Exception e) {
            status = GpxParseStatus.PARSE_ERROR;
            Log.e(GpxParseManager.GPX_PARSE_LOG_TAG, "Error: An GpxParseManager " + e.getMessage());
        } finally {
            try {
                mStream.close();
            } catch (Exception e) {
                Log.e(GpxParseManager.GPX_PARSE_LOG_TAG, "Error: An GpxParseManager close stream" + e.getMessage());
            }
        }
        if (status == GpxParseStatus.RUNNING) {
            status = GpxParseStatus.SUCCESS;
        }

        return status;
    }

    private GpxParseStatus generateTrackInfoList(){
        if (mGpx == null){
            return GpxParseStatus.PARSE_ERROR;
        }

        try {
            List<TrackInfo> resultTracks = new ArrayList<>();

            Set<Track> tracks = mGpx.getTracks();
            for (Track track : tracks){
                TrackInfo trackInfo = new TrackInfo(track);
                resultTracks.add(trackInfo);
            }

            mTracks = resultTracks;
        } catch (Exception e){
            Log.e(GpxParseManager.GPX_PARSE_LOG_TAG, "Error: An GpxParseManager in generateTrackInfoList " + e.getMessage());
            return GpxParseStatus.TRACKS_GENERATE_ERROR;
        }

        return GpxParseStatus.SUCCESS;
    }

    public static class GpxParseResult implements Serializable {
        private List<TrackInfo> mTracks;
        private GPX mGpx;

        public List<TrackInfo> getTracks() {
            return mTracks;
        }

        public GPX getGpx() {
            return mGpx;
        }
    }
}
