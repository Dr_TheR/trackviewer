package ru.worldtillfuture.trackviewer.gpx.parse;

import android.support.annotation.StringRes;

import java.io.Serializable;

import ru.worldtillfuture.trackviewer.R;

/**
 * Author: Ignatov Pavel
 * Date: 07.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public enum GpxParseStatus implements Serializable{
    IS_NOT_STARTED(0, R.string.is_not_started),
    RUNNING(1, R.string.running),
    SUCCESS(2, R.string.success),
    FILE_NOT_FOUND(-1, R.string.file_not_found),
    TRACKS_GENERATE_ERROR(-2, R.string.tracks_generate_error),
    PARSE_ERROR(-3, R.string.parse_error);


    private int mValue;
    private int mMessageRes;

    public int getValue() {
        return mValue;
    }

    public int getMessageRes() {
        return mMessageRes;
    }

    GpxParseStatus(int value, @StringRes int messageRes){
        mValue = value;
        mMessageRes = messageRes;
    }
}
