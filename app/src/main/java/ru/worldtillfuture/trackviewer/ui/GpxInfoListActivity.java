package ru.worldtillfuture.trackviewer.ui;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.GpxInfo;

public class GpxInfoListActivity extends ActionBarActivity implements
        GpxInfoListActivityFragment.OnGpxInfoSelectListener{
    private static final String GPX_INFO_LIST_FRAGMENT_TAG = "gpx_info_list_fragment";
    private static final String ABOUT_DIALOG_TAG = "about_dialog";

    GpxInfoListActivityFragment mGpxInfoListFragment;
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpx_info_list);

        FragmentManager fragmentManager = getFragmentManager();
        mGpxInfoListFragment =  (GpxInfoListActivityFragment) fragmentManager.findFragmentByTag(GPX_INFO_LIST_FRAGMENT_TAG);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        mGpxInfoListFragment.setOnGpxInfoSelectListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_gpx_info_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                DialogFragment aboutDialog = new AboutDialog();
                aboutDialog.show(getFragmentManager(), ABOUT_DIALOG_TAG);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnGpxInfoSelect(GpxInfo gpxInfo) {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(MapActivity.EXTRA_GPX_INFO, gpxInfo);
        startActivity(intent);
    }
}
