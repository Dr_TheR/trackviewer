package ru.worldtillfuture.trackviewer.ui;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.util.List;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.database.DatabaseManager;
import ru.worldtillfuture.trackviewer.ui.adapter.GpxInfoListAdapter;
import ru.worldtillfuture.trackviewer.utilities.DeviceUtilities;
import ru.worldtillfuture.trackviewer.utilities.FileUtilities;

/**
 * A placeholder fragment containing a simple view.
 */
public class GpxInfoListActivityFragment extends Fragment implements
        GpxInfoListAdapter.OnGpxInfoClickListener, View.OnClickListener{
    public static final int REQUEST_CODE_CHOICE_FILE = 1;
    public static final int RESULT_CODE_OK = -1;

    List<GpxInfo> mGpxInfoList;
    private GpxInfoListAdapter mAdapter;
    private RecyclerView mGpxInfoListView;
    private OnGpxInfoSelectListener mGpxInfoSelectListener;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton mAddGpxInfoButton;
    private ItemTouchHelper mItemTouchHelper;

    public static GpxInfoListActivityFragment newInstance(){
        GpxInfoListActivityFragment fragment = new GpxInfoListActivityFragment();

        return fragment;
    }

    public void setOnGpxInfoSelectListener(OnGpxInfoSelectListener gpxInfoSelectListener) {
        this.mGpxInfoSelectListener = gpxInfoSelectListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (mGpxInfoList == null)
            mGpxInfoList = DatabaseManager.getGpxInfoDAO().getAllElements();

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                GpxInfoListAdapter.ViewHolder holder = (GpxInfoListAdapter.ViewHolder) viewHolder;
                GpxInfo gpxInfo = holder.getGpxInfo();
                mAdapter.removeGpxInfo(mAdapter.getPositionById(gpxInfo.getId()));
                DatabaseManager.getGpxInfoDAO().deleteElement(gpxInfo);
            }
        };

        mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        mAdapter = new GpxInfoListAdapter(mGpxInfoList);
        mAdapter.setOnGpxInfoClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gpx_info_list, container, false);
        mGpxInfoListView = (RecyclerView) rootView.findViewById(R.id.gpx_info_list);
        mAddGpxInfoButton = (FloatingActionButton) rootView.findViewById(R.id.add_gpx_info_button);

        mAddGpxInfoButton.setOnClickListener(this);

        mGpxInfoListView.setHasFixedSize(true);
        mGpxInfoListView.setAdapter(mAdapter);
        mLayoutManager = new StaggeredGridLayoutManager(DeviceUtilities.getDisplayColumns(getActivity()), StaggeredGridLayoutManager.VERTICAL);
        mGpxInfoListView.setLayoutManager(mLayoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        mGpxInfoListView.setItemAnimator(itemAnimator);

        mItemTouchHelper.attachToRecyclerView(mGpxInfoListView);
        return rootView;
    }

    @Override
    public void onGpxInfoClick(GpxInfo gpxInfo, int position) {
        updateDateGpxInfo(gpxInfo);
        if (mGpxInfoSelectListener != null){
            mGpxInfoSelectListener.OnGpxInfoSelect(gpxInfo);
        }
    }

    private void startChoiceFile(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_CODE_CHOICE_FILE);
    }

    @Override
    public void onClick(View v) {
        startChoiceFile();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CHOICE_FILE && resultCode == RESULT_CODE_OK){
            processingFileUri(data.getData());
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private GpxInfo generateGpxInfo(File file){
        for (GpxInfo gpxInfo : mGpxInfoList){
            if (gpxInfo.getFilePath().equals(file.getAbsolutePath()))
                return gpxInfo;
        }

        return new GpxInfo(file.getAbsolutePath(), FileUtilities.getFileNameWithoutExtension(file.getName()));
    }

    public void processingFileUri(Uri fileURI){
        File file = new File(fileURI.getPath());
        String name = file.getName();
        boolean isGpxFile = FileUtilities.getFileExtension(name).toUpperCase().equals(FileUtilities.GPX);
        if (isGpxFile) {
            GpxInfo gpxInfo = generateGpxInfo(file);
            if (!mGpxInfoList.contains(gpxInfo))
                mAdapter.addGpxInfo(gpxInfo);
            updateDateGpxInfo(gpxInfo);
        }
        else
            Toast.makeText(getActivity(), R.string.is_not_gpx, Toast.LENGTH_LONG).show();
    }

    private void updateDateGpxInfo(GpxInfo gpxInfo){
        gpxInfo.updateDate();
        DatabaseManager.getGpxInfoDAO().saveElement(gpxInfo);
        int position = mAdapter.getPositionById(gpxInfo.getId());
        if (position != -1) {
            mAdapter.notifyItemChanged(position);
            mAdapter.moveToTopGpxInfo(position);
        }
    }

    public interface OnGpxInfoSelectListener {
        void OnGpxInfoSelect(GpxInfo gpxInfo);
    }
}
