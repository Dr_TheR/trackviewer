package ru.worldtillfuture.trackviewer.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.urizev.gpx.beans.GPX;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.data.TrackInfo;
import ru.worldtillfuture.trackviewer.gpx.parse.GpxParseHandler;
import ru.worldtillfuture.trackviewer.gpx.parse.GpxParseManager;
import ru.worldtillfuture.trackviewer.gpx.parse.GpxParseProcessing;
import ru.worldtillfuture.trackviewer.gpx.parse.GpxParseStatus;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class MapActivity extends Activity implements
        GpxParseHandler.GpxParseCallback,
        TrackInfoListFragment.OnTrackSelectListener {
    private static final String MAP_FRAGMENT_TAG = "map";
    private static final String TRACK_INFO_LIST_FRAGMENT_TAG = "track_info_list_fragment";

    public static final String EXTRA_GPX_INFO = "gpx_info";

    private MapFragment mMapFragment;
    private TrackInfoListFragment mTrackInfoListFragment;
    private GoogleMap mMap;
    private GpxInfo mGpxInfo;
    private GPX mGpx;
    private GpxParseProcessing mParseProcessing;
    private List<TrackInfo> mTracks;
    private SlidingUpPanelLayout mSlidingPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        FragmentManager fragmentManager = getFragmentManager();
        mMapFragment =  (MapFragment) fragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG);
        mTrackInfoListFragment =  (TrackInfoListFragment) fragmentManager.findFragmentByTag(TRACK_INFO_LIST_FRAGMENT_TAG);

        mSlidingPanel = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mSlidingPanel.setScrollableView(mTrackInfoListFragment.getListView());
        mSlidingPanel.setEnabled(false);

        mTrackInfoListFragment.setOnTrackSelectListener(this);

        mMap = mMapFragment.getMap();


        restoreData();

        boolean isParsingNeedRestart = mParseProcessing != null
                && mParseProcessing.getStatus() != GpxParseStatus.SUCCESS
                && mParseProcessing.getStatus() != GpxParseStatus.RUNNING;
        if (mParseProcessing == null || isParsingNeedRestart) {
            mGpxInfo = getGpxInfo();
            parseGpx(mGpxInfo);
        }
        else{
            redrawTracks();
        }
    }

    private void restoreData(){
        StateInstance instance = (StateInstance) getLastNonConfigurationInstance();
        if (instance != null) {
            mGpxInfo = instance.mGpxInfo;
            mGpx = instance.mGpx;
            mTracks = instance.mTrackInfoList;
            mParseProcessing = instance.mParseProcessing;
            if (mParseProcessing.getStatus() == GpxParseStatus.RUNNING)
                mParseProcessing.setHandler(new GpxParseHandler(this));
        }
    }

    private GpxInfo getGpxInfo(){
        GpxInfo gpxInfo = mGpxInfo;
        if (gpxInfo == null) {
            Intent intent = getIntent();
            gpxInfo = (GpxInfo) intent.getSerializableExtra(EXTRA_GPX_INFO);
            if(gpxInfo == null){
                finish();
            }
        }

        return gpxInfo;
    }

    private void parseGpx(GpxInfo gpxInfo){
        File gpxFile = new File(gpxInfo.getFilePath());
        Handler handler = new GpxParseHandler(this);
        mParseProcessing = GpxParseManager.parseGPX(gpxFile, handler);
    }

    private void redrawTracks(){
        if (mParseProcessing.getStatus() == GpxParseStatus.SUCCESS){
            if (mTracks == null || mGpx == null) {
                mTracks = mParseProcessing.getTracks();
                mGpx = mParseProcessing.getGpx();
            }
        }

        drawTracks(mTracks);
    }

    private void drawTracks(List<TrackInfo> tracks){
        if (tracks != null) {
            for (TrackInfo track : tracks) {
                displayTrack(track);
            }
            mSlidingPanel.setEnabled(true);
            mTrackInfoListFragment.setTracks(tracks);
        }
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        StateInstance instance = new StateInstance();
        instance.mGpx = mGpx;
        instance.mGpxInfo = mGpxInfo;
        instance.mTrackInfoList = mTracks;
        instance.mParseProcessing = mParseProcessing;
        return instance;
    }

    private void displayTrack(TrackInfo track){
        if (mMap == null)
            return;

        PolylineOptions options = new PolylineOptions()
                .width(10)
                .color(track.getColor())
                .geodesic(true)
                .zIndex(100);
        track.setTrackPolyline(mMap.addPolyline(options));

        MarkerOptions markerOptions = new MarkerOptions()
                .position(track.getStartPoint())
                .title("Начало " + track.getTrack().getName());
        track.setStartMarker(mMap.addMarker(markerOptions));

        markerOptions
                .position(track.getFinishPoint())
                .title("Конец " + track.getTrack().getName());
        track.setFinishMarker(mMap.addMarker(markerOptions));
    }

    @Override
    public void onParsingSuccess(GPX gpx, List<TrackInfo> tracks) {
        mGpx = gpx;
        mTracks = tracks;

        if (mGpx != null){
            mTrackInfoListFragment.setHeaderInfo(mGpxInfo.getName(), gpx.getCreator());
        }

        drawTracks(tracks);

        if (tracks != null && tracks.size() > 0){
            selectTrack(tracks.get(0));
        }
    }

    public void selectTrack(TrackInfo trackInfo){
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(trackInfo.getBounds(), 50));
    }

    @Override
    public void onParsingError(GpxParseStatus status) {
        Toast.makeText(this, status.getMessageRes(), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onTrackSelect(TrackInfo track) {
        mSlidingPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        selectTrack(track);
    }

    public class StateInstance implements Serializable {
        private GpxInfo mGpxInfo;
        private GPX mGpx;
        private List<TrackInfo> mTrackInfoList;
        private GpxParseProcessing mParseProcessing;
    }
}
