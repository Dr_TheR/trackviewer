package ru.worldtillfuture.trackviewer.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.TrackInfo;
import ru.worldtillfuture.trackviewer.ui.adapter.TrackInfoListAdapter;

/**
 * Author: Ignatov Pavel
 * Date: 07.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class TrackInfoListFragment extends Fragment implements
        AdapterView.OnItemClickListener{
    private List<TrackInfo> mTracks;
    private TrackInfoListAdapter mAdapter;
    private ListView mTrackInfoListView;
    private OnTrackSelectListener mTrackSelectListener;
    private String mAuthor;
    private String mFileName;
    private TextView mAuthorView;
    private TextView mFileNameView;
    private View mHeaderInfo;
    private View mHeaderLoader;

    public static TrackInfoListFragment newInstance(){
        TrackInfoListFragment fragment = new TrackInfoListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);

        mAdapter = new TrackInfoListAdapter(getActivity());
    }

    public View getListView(){
        return mTrackInfoListView;
    }

    public void setTracks(List<TrackInfo> tracks){
        if (tracks == null){
            tracks = new ArrayList<>();
        }

        mTracks = tracks;
        mAdapter.setTracks(tracks);
        setHeaderLoaderMode(mTracks == null);
    }

    public void setHeaderInfo(String fileName, String author){
        if (fileName == null || fileName.isEmpty())
            fileName = getActivity().getString(R.string.name_default);
        mFileName = fileName;

        if (author == null || author.isEmpty())
            author = getActivity().getString(R.string.author_default);
        mAuthor = author;


        mFileNameView.setText(fileName);
        mAuthorView.setText(author);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_track_info_list, container, false);
        mTrackInfoListView = (ListView) rootView.findViewById(R.id.track_info_list);

        mFileNameView = (TextView) rootView.findViewById(R.id.file_name);
        mAuthorView = (TextView) rootView.findViewById(R.id.author);

        mHeaderInfo = rootView.findViewById(R.id.header_info);
        mHeaderLoader = rootView.findViewById(R.id.header_loader);

        setHeaderLoaderMode(mTracks == null);

        mFileNameView.setText(mFileName);
        mAuthorView.setText(mAuthor);

        mTrackInfoListView.setAdapter(mAdapter);
        mTrackInfoListView.setOnItemClickListener(this);

        return rootView;
    }

    private void setHeaderLoaderMode(boolean isLoaderMode){
        if (isLoaderMode){
            mHeaderLoader.setVisibility(View.VISIBLE);
            mHeaderInfo.setVisibility(View.INVISIBLE);
        }
        else{
            mHeaderLoader.setVisibility(View.INVISIBLE);
            mHeaderInfo.setVisibility(View.VISIBLE);
        }
    }

    public void setOnTrackSelectListener(OnTrackSelectListener trackSelectListener) {
        this.mTrackSelectListener = trackSelectListener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TrackInfo track = mTracks.get(position);

        if(track != null)
            mTrackSelectListener.onTrackSelect(track);
    }

    public interface OnTrackSelectListener{
        void onTrackSelect(TrackInfo track);
    }
}
