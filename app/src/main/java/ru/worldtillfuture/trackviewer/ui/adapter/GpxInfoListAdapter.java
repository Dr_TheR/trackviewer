package ru.worldtillfuture.trackviewer.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.utilities.DateUtilities;

/**
 * Author: Ignatov Pavel
 * Date: 05.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class GpxInfoListAdapter extends RecyclerView.Adapter<GpxInfoListAdapter.ViewHolder> implements
        View.OnClickListener {
    private List<GpxInfo> mGpxInfoList;
    private OnGpxInfoClickListener mGpxInfoClickListener;

    public GpxInfoListAdapter(List<GpxInfo> gpxInfoList) {
        this.mGpxInfoList = gpxInfoList;
    }

    public void setOnGpxInfoClickListener(OnGpxInfoClickListener gpxInfoClickListener){
        this.mGpxInfoClickListener = gpxInfoClickListener;
    }

    public void addGpxInfo(GpxInfo gpxInfo){
        mGpxInfoList.add(0, gpxInfo);
        notifyItemInserted(0);
    }

    public void removeGpxInfo(int position){
        mGpxInfoList.remove(position);
        notifyItemRemoved(position);
    }

    public void moveToTopGpxInfo(int position){
        GpxInfo gpxInfo = mGpxInfoList.get(position);
        mGpxInfoList.remove(position);
        mGpxInfoList.add(0, gpxInfo);
        notifyItemMoved(position, 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gpx_info, viewGroup, false);
        view.setOnClickListener(this);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        GpxInfo gpxInfo = mGpxInfoList.get(i);
        viewHolder.setGpxInfo(gpxInfo);
    }



    @Override
    public int getItemCount() {
        return mGpxInfoList.size();
    }

    @Override
    public void onClick(View v) {
        int id = (int) v.getTag();
        int position = getPositionById(id);
        GpxInfo gpxInfo = mGpxInfoList.get(position);
        if (mGpxInfoClickListener != null){
            mGpxInfoClickListener.onGpxInfoClick(gpxInfo, position);
        }
    }

    public int getPositionById(int id){
        for (int i = 0; i < mGpxInfoList.size(); i++){
            GpxInfo gpxInfo = mGpxInfoList.get(i);
            if (gpxInfo.getId() == id){
                return i;
            }
        }

        return -1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private GpxInfo gpxInfo;
        private TextView lastActivityDate;
        private TextView name;

        public GpxInfo getGpxInfo() {
            return gpxInfo;
        }

        public void setGpxInfo(GpxInfo gpxInfo){
            this.gpxInfo = gpxInfo;
            view.setTag(gpxInfo.getId());
            name.setText(gpxInfo.getName());
            lastActivityDate.setText(DateUtilities.getString(gpxInfo.getLastActivityDate()));
        }

        public ViewHolder(View view) {
            super(view);
            this.view = view;

            lastActivityDate = (TextView) view.findViewById(R.id.last_activity_date);
            name = (TextView) view.findViewById(R.id.name);
        }
    }

    public interface OnGpxInfoClickListener{
        void onGpxInfoClick(GpxInfo gpxInfo, int position);
    }
}
