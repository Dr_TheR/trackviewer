package ru.worldtillfuture.trackviewer.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import ru.worldtillfuture.trackviewer.R;
import ru.worldtillfuture.trackviewer.data.GpxInfo;
import ru.worldtillfuture.trackviewer.data.TrackInfo;
import ru.worldtillfuture.trackviewer.utilities.ColorUtilities;
import ru.worldtillfuture.trackviewer.utilities.DateUtilities;

/**
 * Author: Ignatov Pavel
 * Date: 07.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class TrackInfoListAdapter extends BaseAdapter{
    private List<TrackInfo> mTracks = new ArrayList<>();
    private final Context mContext;

    public TrackInfoListAdapter(Context context) {
        mContext = context;
    }

    public void setTracks(List<TrackInfo> tracks) {
        mTracks = tracks;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mTracks.size();
    }

    @Override
    public Object getItem(int position) {
        return mTracks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mTracks.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track_info, parent, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.setTrack(mTracks.get(position), mContext);


        return convertView;
    }

    public static class ViewHolder{
        private TrackInfo track;
        private TextView name;
        private TextView description;
        private ImageView color;

        public void setTrack(TrackInfo track, Context context){
            this.track = track;

            String strName = track.getTrack().getName();
            if(strName == null || strName.isEmpty())
                strName = context.getString(R.string.name_default);
            name.setText(strName);

            String strDescription = track.getTrack().getDescription();
            if(strDescription == null || strDescription.isEmpty())
                strDescription = context.getString(R.string.description_default);
            description.setText(strDescription);

            color.setImageBitmap(ColorUtilities.drawColoredCircle(track.getColor()));
        }

        public ViewHolder(View view) {
            description = (TextView) view.findViewById(R.id.description);
            name = (TextView) view.findViewById(R.id.name);
            color = (ImageView) view.findViewById(R.id.color);
        }
    }
}
