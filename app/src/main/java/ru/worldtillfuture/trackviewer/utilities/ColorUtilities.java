package ru.worldtillfuture.trackviewer.utilities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * Author: Ignatov Pavel
 * Date: 08.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class ColorUtilities {
    public static int generateRandomColor() {
        Random random = new Random();
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);

        float[] hsv = new float[3];
        float hue = random.nextFloat();//Оттенок
        float saturation =  0.9f;//Насыщенность
        float luminance = 1.0f;//Яркость
        hsv[0] = hue;
        hsv[1] = saturation;
        hsv[2] = luminance;

        Color.RGBToHSV(red, green, blue, hsv);

        return Color.HSVToColor(hsv);
    }

    public static Bitmap drawColoredCircle(int color){
        Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);

        bitmap = bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(8f);
        canvas.drawCircle(50, 50, 35, paint);

        return bitmap;
    }
}
