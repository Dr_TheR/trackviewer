package ru.worldtillfuture.trackviewer.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author: Ignatov Pavel
 * Date: 06.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class DateUtilities {
    public static String getString(Date date){
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
            return dateFormat.format(date);
        }
        else
            return "";
    }
}
