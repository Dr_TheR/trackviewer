package ru.worldtillfuture.trackviewer.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

/**
 * Author: Ignatov Pavel
 * Date: 06.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class DeviceUtilities {
    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static int getDisplayColumns(Activity activity) {
        int columnCount = 1;
        if (isTablet(activity)) {
            columnCount = 2;
        }
        return columnCount;
    }
}
