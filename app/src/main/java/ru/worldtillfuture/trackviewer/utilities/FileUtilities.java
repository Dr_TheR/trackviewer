package ru.worldtillfuture.trackviewer.utilities;

/**
 * Author: Ignatov Pavel
 * Date: 06.09.2015
 * E-mail: workignatovpavel@gmail.com
 */
public class FileUtilities {
    public static final String GPX = "GPX";
    public static String getFileExtension(String fileName) {
        return fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
    }

    public static String getFileNameWithoutExtension(String fileName) {
        return fileName.substring( 0, fileName.lastIndexOf("."));
    }
}
